#pragma once



#ifndef STEMBUSSLAGER
#define STEMBUSSLAGER



#include "ofMain.h"
#include "Game.h"
#include "ofxJSONElement.h"
#include <iostream>



#define NUM_FRAMES 24
#define NUM_BYTES_IN 3
#define NUM_BYTES_OUT 10



class testApp : public ofBaseApp {

public:
    void setup();
    void update();
    void draw();
    void keyReleased(int key);
    
    ofxJSONElement result;
    
private:
    
    // ARDUINO
    bool bSendSerialMessage;
    unsigned char bytesReturned[NUM_BYTES_IN];
    int countCycles;
    ofSerial serial;
    
    // GAME
    Game* game;
    
    // TEXT
    ofTrueTypeFont msg;

};



#endif
