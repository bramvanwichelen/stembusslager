#include "testApp.h"



void testApp::setup(){
    
    // GENERAL
    ofSetFrameRate(NUM_FRAMES);
    ofSetWindowTitle("STEMBUSSLAGER");
	ofSetVerticalSync(true);
    
    // ARDUINO
    countCycles = 0;
    bSendSerialMessage = true;
    serial.enumerateDevices();
    serial.setup(0, 115200);
    
    // GAME
    game = new Game();
	
    
    
    std::string file = "engagor.json";
	
	// Now parse the JSON
	bool parsingSuccessful = result.open(file);
	
    if (parsingSuccessful) {
		cout << result.getRawString() << endl;
        
        // now write pretty print
        if(!result.save("example_output_pretty.json",true)) {
            cout << "example_output_pretty.json written unsuccessfully." << endl;
        } else {
            cout << "example_output_pretty.json written successfully." << endl;
        }
        
        // now write without pretty print
        if(!result.save("example_output_fast.json",false)) {
            cout << "example_output_pretty.json written unsuccessfully." << endl;
        } else {
            cout << "example_output_pretty.json written successfully." << endl;
        }
		
	} else {
		cout  << "Failed to parse JSON" << endl;
	}
    
    
}



void testApp::update() {
    
    // ARDUINO WRITE
    if (bSendSerialMessage) {
        
        // score_1 + score_2 + score_3
        int score = game->score;
        string score_string = "";
        if (score<100) score_string += "0";
        if (score<10) score_string += "0";
        score_string += ofToString(score);
        const char * score_chars = score_string.c_str();
        char s1 = score_chars[0];
        char s2 = score_chars[1];
        char s3 = score_chars[2];
        
        // lives
        int lives = game->lives;
        string lives_string = ofToString(lives);
        const char * lives_chars = lives_string.c_str();
        char l = lives_chars[0];
        
        // ranking_1 + ranking_2 + ranking_3
        int ranking = game->rank + 1;
        string ranking_string = "";
        if (ranking<100) ranking_string += "0";
        if (ranking<10) ranking_string += "0";
        ranking_string += ofToString(ranking);
        const char * ranking_chars = ranking_string.c_str();
        char r1 = ranking_chars[0];
        char r2 = ranking_chars[1];
        char r3 = ranking_chars[2];
        
        // topscore_1 + topscore_2 + topscore_3
        int topscore = game->topscore;
        string topscore_string = "";
        if (topscore<100) topscore_string += "0";
        if (topscore<10) topscore_string += "0";
        topscore_string += ofToString(topscore);
        const char * topscore_chars = topscore_string.c_str();
        char t1 = topscore_chars[0];
        char t2 = topscore_chars[1];
        char t3 = topscore_chars[2];
        
        // send string
        
        // vervangen door score_string!?
        unsigned char buf[NUM_BYTES_OUT] = {s1, s2, s3, l, t1, t2, t3, r1, r2, r3};
        serial.writeBytes(&buf[0], NUM_BYTES_OUT);
        
        bSendSerialMessage = false;
    }
    
    if(++countCycles == NUM_FRAMES) {
        bSendSerialMessage = true;
        countCycles = 0;
    }
    
    // ARDUINO READ
    unsigned char bytesReturned[NUM_BYTES_IN];
    memset(bytesReturned, 0, NUM_BYTES_IN);
    while (serial.readBytes(bytesReturned, NUM_BYTES_IN) > 0) {};
    
    string plx = ofToString(bytesReturned);
    
    if (plx.length()>0) {
        cout << "input: " << plx << endl;
        
        switch (ofToChar(plx)) {
            case 'm': game->hitMultiPlayer(); break; // m = multiplayer
            case 'n': game->hitSinglePlayer(); break; // n = singleplayer
            case 'p': game->hitSinglePlayer(); break; // p = re/play
            case 'f': game->hitPlexi(0); break; // a = plexi 1
            case 'a': game->hitPlexi(1); break; // z = plexi 2
            case 'k': game->hitPlexi(2); break; // e = plexi 3
            case 'g': game->hitPlexi(3); break; // r = plexi 4
            case 'e': game->hitPlexi(4); break; // q = plexi 5
            case 'c': game->hitPlexi(5); break; // s = plexi 6
            case 'l': game->hitPlexi(6); break; // d = plexi 7
            case 'i': game->hitPlexi(7); break; // f = plexi 8
            case 'd': game->hitPlexi(8); break; // w = plexi 9
            case 'b': game->hitPlexi(9); break; // x = plexi 10
            case 'h': game->hitPlexi(10); break; // c = plexi 11
            case 'j': game->hitPlexi(11); break; // v = plexi 12
        }
    }
    //    }
    //    else {
    //        cout << "(re)try serial.setup" << endl;
    //        serial.setup(0, 115200);
    //    }
    
    game->update();
}



void testApp::draw() {
    ofBackground(0);
    
    
    ofBackground(0, 0, 0);
    
    game->draw();
    
    ofColor(255, 255, 255);
    msg.loadFont("Arial.ttf", 15);
    msg.drawString(game->action, 20, 755);

	
    ofSetHexColor(0x00FF00);
	
    std::stringstream ss;
    
    //    {
    //        "firstName": "John",
    //        "lastName": "Smith",
    
    ss << "firstName          = " << result["firstName"].asString() << endl;
    ss << "lastName           = " << result["lastName"].asString() << endl;
    
    //        "age": 25,
    
    ss << "age                = " << result["age"].asDouble() << endl;
    
    //        "address": {
    //            "streetAddress": "21 2nd Street",
    //            "city": "New York",
    //            "state": "NY",
    //            "postalCode": "10021"
    //        },
    
    ss << "streetAddress      = " << result["address"]["streetAddress"].asString() << endl;
    ss << "city               = " << result["address"]["city"].asString() << endl;
    ss << "state              = " << result["address"]["state"].asString() << endl;
    ss << "postalCode         = " << result["address"]["postalCode"].asString() << endl;
    
    
    //        "phoneNumber": [
    //                        {
    //                        "type": "home",
    //                        "number": "212 555-1234"
    //                        },
    //                        {
    //                        "type": "fax",
    //                        "number": "646 555-4567"
    //                        }
    //                        ]
    //    }
    
    int i = 0;
    
    ss << "phoneNumber:type   = " << result["phoneNumber"][i]["type"].asString() << endl;
    ss << "phoneNumber:number = " << result["phoneNumber"][i]["number"].asString() << endl;
    ss << "phoneNumber:type   = " << result["phoneNumber"][i+1]["type"].asString() << endl;
    ss << "phoneNumber:number = " << result["phoneNumber"][i+1]["number"].asString() << endl;
    
    ofDrawBitmapString(ss.str(), 10, 14);
    
}



void testApp::keyReleased(int key) {
    
    if (key==OF_KEY_CONTROL) {
        game->setup = !game->setup;
    }
    
    if (game->setup) {
        switch (key) {
            case 'q': game->selectPlexi(0); break; // a = plexi 1
            case 'w': game->selectPlexi(1); break; // z = plexi 2
            case 'e': game->selectPlexi(2); break; // e = plexi 3
            case 'r': game->selectPlexi(3); break; // r = plexi 4
            case 'a': game->selectPlexi(4); break; // q = plexi 5
            case 's': game->selectPlexi(5); break; // s = plexi 6
            case 'd': game->selectPlexi(6); break; // d = plexi 7
            case 'f': game->selectPlexi(7); break; // f = plexi 8
            case 'z': game->selectPlexi(8); break; // w = plexi 9
            case 'x': game->selectPlexi(9); break; // x = plexi 10
            case 'c': game->selectPlexi(10); break; // c = plexi 11
            case 'v': game->selectPlexi(11); break; // v = plexi 12
            case OF_KEY_UP: game->movePlexi("y", -1); break;
            case OF_KEY_DOWN: game->movePlexi("y", 1); break;
            case OF_KEY_LEFT: game->movePlexi("x", -1); break;
            case OF_KEY_RIGHT: game->movePlexi("x", 1); break;
            case OF_KEY_COMMAND: game->resizePlexi(1); break;
            case OF_KEY_ALT: game->resizePlexi(-1); break;
        }
    }
    else {
        switch (key) {
            case 'm': game->hitMultiPlayer(); break; // m = multiplayer
            case 'n': game->hitSinglePlayer(); break; // n = singleplayer
            case 'p': game->hitSinglePlayer(); break; // p = re/play
            case 'q': game->hitPlexi(0); break; // a = plexi 1
            case 'w': game->hitPlexi(1); break; // z = plexi 2
            case 'e': game->hitPlexi(2); break; // e = plexi 3
            case 'r': game->hitPlexi(3); break; // r = plexi 4
            case 'a': game->hitPlexi(4); break; // q = plexi 5
            case 's': game->hitPlexi(5); break; // s = plexi 6
            case 'd': game->hitPlexi(6); break; // d = plexi 7
            case 'f': game->hitPlexi(7); break; // f = plexi 8
            case 'z': game->hitPlexi(8); break; // w = plexi 9
            case 'x': game->hitPlexi(9); break; // x = plexi 10
            case 'c': game->hitPlexi(10); break; // c = plexi 11
            case 'v': game->hitPlexi(11); break; // v = plexi 12
        }
    }
}


